<!DOCTYPE html>
<html>
<head>
  <style>
    .value {
      font-weight: bold;
      padding-left: 20px;
    }
  </style>
</head>

<body class="document">
<div class="page" style="padding-top: 0">
  <div style="font-weight: bold; text-align: center">
    Example Certificate
  </div>

  <table border="0" cellspacing="0" cellpadding="2px" bgcolor="white">
    <th>
      <td colspan="2">This is to demonstrate how to create a certificate to push to Digital Locker.</td>
    </th>
    <tr>
      <td>This certificate is issued to </td>
      <td class="value"><?php echo $content['title']; ?></td>
    </tr>
  </table>
</div>
</body>
</html>
