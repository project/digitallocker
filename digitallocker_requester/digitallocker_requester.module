<?php

/**
 * Implements hook_field_widget_info_alter().
 */
function digitallocker_requester_field_widget_info_alter(&$info) {
  if (isset($info['file_generic'])) {
    $info['file_generic']['settings']['digitallocker_enable'] = 1;
    $info['file_generic']['settings']['digitallocker_exclusive'] = 0;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function digitallocker_requester_form_field_ui_field_edit_form_alter(&$form, $form_state) {
  if (empty($form['#field']['locked'])
    and isset($form['#instance']['widget']['settings']['digitallocker_enable'])
    and isset($form['#instance']['widget']['settings']['digitallocker_exclusive'])
  ) {

    $form['instance']['widget']['settings']['digitallocker_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Digital Locker on this field.'),
      '#default_value' => $form['#instance']['widget']['settings']['digitallocker_enable'],
      '#weight' => 10,
    );

    $form['instance']['widget']['settings']['digitallocker_exclusive'] = array(
      '#type' => 'checkbox',
      '#title' => t('Limit uploads exclusively to Digital Locker.'),
      '#default_value' => $form['#instance']['widget']['settings']['digitallocker_exclusive'],
      '#weight' => 10,
    );
  }
}

/**
 * Implements hook_element_info_alter().
 */
function digitallocker_requester_element_info_alter(&$type) {
  $type['managed_file']['#process'][] = 'digitallocker_process';
  $type['managed_file']['#pre_render'][] = 'digitallocker_pre_render';
}

function digitallocker_process($element, &$form_state, $form) {
  if (isset($element['#digitallocker']) and
    $element['#digitallocker']['enable'] == 1 and
    $element['#default_value']['fid'] == 0
  ) {
    $element['digitallocker'] = array(
      '#markup' => '<div' . drupal_attributes(array(
          'class' => 'share_fm_dl',
          'id' => $element['#id'] . '-digitallocker',
        )) . '></div>',
      '#weight' => $element['upload_button']['#weight'] + 1,
    );
  }
  return $element;
}

function digitallocker_pre_render($element) {
  if (isset($element['#digitallocker'])
    and $element['#digitallocker']['enable'] == 1
    and $element['#digitallocker']['exclusive'] == 1
  ) {
    $element['upload']['#access'] = FALSE;
    $element['upload_button']['#access'] = FALSE;
  }
  return $element;
}

/**
 * Implements hook_field_widget_form_alter().
 */
function digitallocker_requester_field_widget_form_alter(&$element, &$form_state, $context) {
  if (isset($context['instance']['widget']['settings']['digitallocker_enable'])) {
    foreach ($element as $key => &$item) {
      if (!is_int($key)) {
        continue;
      }
      $item['#digitallocker']['enable'] = $context['instance']['widget']['settings']['digitallocker_enable'];
      $item['#digitallocker']['exclusive'] = $context['instance']['widget']['settings']['digitallocker_exclusive'];
    }
  }
}

/**
 * Implements hook_page_build().
 */
function digitallocker_requester_page_build(&$page) {
  $requester_id = variable_get('digitallocker_requester_requester_id');
  $secret_key = variable_get('digitallocker_requester_secret_key');
  $timstamp = date_timestamp_get(date_create());
  $hash_key = hash('sha256', $requester_id . $secret_key . $timstamp);

  $page['content']['digitallocker_script'] = array(
    '#weight' => 100,
    '#markup' => '<script' .
      drupal_attributes(array(
        'id' => 'dlshare',
        'src' => variable_get('digitallocker_requester_base_url') . '/requester/api/2/dl.js',
        'type' => 'text/javascript',
        'async' => 'async',
        'time-stamp' => $timstamp,
        'data-app-id' => $requester_id,
        'data-app-hash' => $hash_key,
        'data-callback' => 'digitallocker_requester_callback',
      )) .
      '></script>'
  );
  drupal_add_js(array('DigitalLocker' => array('callbackUrl' => url('digitallocker/requester/callback'))), 'setting');
}

/**
 * Implements hook_menu().
 */
function digitallocker_requester_menu() {
  return array(
    'admin/config/digitallocker' => array(
      'title' => 'Digital Locker',
      'description' => 'Digital Locker Configurations.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file' => 'system.admin.inc',
      'file path' => drupal_get_path('module', 'system'),
    ),
    'admin/config/digitallocker/requester' => array(
      'title' => 'DL Requester',
      'description' => 'Configure the Digital Locker Settings for Requester Functionality.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('digitallocker_requester_settings'),
      'access arguments' => array('administer site configuration'),
    ),
    'digitallocker/requester/callback' => array(
      'type' => MENU_CALLBACK,
      'page callback' => 'digitallocker_requester_page_callback',
      'access callback' => 'digitallocker_requester_access_callback'
    ),
  );
}

function digitallocker_requester_access_callback() {
  if (isset($_POST['data'])) {
    $data = json_decode($_POST['data']);
    if (isset($data->sharedTill) and
      isset($data->txn) and
      isset($data->uri)
    ) {
      return TRUE;
    }
  }
  return FALSE;
}

function digitallocker_requester_page_callback() {
  $data = json_decode($_POST['data']);

  $request = new simpleXMLElement('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><PullDocRequest xmlns:ns2="http://tempuri.org/" />');
  $request->addAttribute('ver', '1.0');
  $request->addAttribute('ts', date('d-m-Y h:i:s', REQUEST_TIME));
  $request->addAttribute('txn', $data->txn);
  $request->addAttribute('orgId', variable_get('digitallocker_issuer_issuer_id'));
  $request->addAttribute('appId', variable_get('digitallocker_requester_requester_id'));
  $request->addAttribute('keyhash', hash('sha256', variable_get('digitallocker_requester_secret_key') . date('d-m-Y h:i:s', REQUEST_TIME)));

  $request->DocDetails->URI = $data->uri;

  $response = drupal_http_request(variable_get('digitallocker_issuer_base_url') . '/public/requestor/api/pulldoc/1/xml', array(
    'method' => 'POST',
    'data' => $request->asXML(),
    'headers' => array(
      'Content-Type' => 'application/xml',
    ),
  ));
  if ($response->code <> 200) {
    watchdog('digital locker', $response->status_message);
  }

  try {
    $response = new simpleXMLElement($response->data);
    if (intval($response->ResponseStatus) != 1) {
      watchdog('digital locker', $response->ResponseStatus);
    }

    $document = base64_decode((string) $response->DocDetails->docContent[0]);
    $file = file_save_data($document, file_default_scheme() . '://' . $data->filename);
    $file->status = 0;
    file_save($file);

    drupal_json_output(array('fid' => $file->fid, 'did' => $data->docId));
  } catch (Exception $e) {
    watchdog('digital locker', $e->getMessage());
  }
  drupal_exit();
}

function digitallocker_requester_settings() {
  $form['digitallocker_requester_requester_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Requester Id'),
    '#description' => t('Set the Requester Id provided by Digital Locker.'),
    '#default_value' => variable_get('digitallocker_requester_requester_id'),
  );

  $form['digitallocker_requester_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#description' => t('Set the Secret Key provided by Digital Locker.'),
    '#default_value' => variable_get('digitallocker_requester_secret_key'),
  );

  $form['digitallocker_requester_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base Url'),
    '#description' => t('Set the Base Url provided by Digital Locker.'),
    '#default_value' => variable_get('digitallocker_requester_base_url'),
  );

  return system_settings_form($form);
}
