<?php
/**
 * @file
 * digitallocker_issuer.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function digitallocker_issuer_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'tokenize_drupal_variables_options';
  $strongarm->value = array(
    'digitallocker_issuer_issuer_id' => 'digitallocker_issuer_issuer_id',
  );
  $export['tokenize_drupal_variables_options'] = $strongarm;

  return $export;
}
