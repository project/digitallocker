<?php
/**
 * @file
 * digitallocker_issuer.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function digitallocker_issuer_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
